import java.math.BigInteger;
import static org.junit.Assert.assertEquals;
/**
 * Created by altair on 15.01.17.
 */
public class Test {

    @org.junit.Test
    public void test(){
        RSA rsa =  new RSA();
        rsa.init(32);

        String[] values = {"13","45","16","1024","5048","500","25407"};
        BigInteger message;

        for (int  i = 0; i < values.length; i++){
            message = new BigInteger(values[i]);
            BigInteger encr = rsa.encrypt(message);
            BigInteger decr = rsa.decrypt(encr);
            assertEquals(message,decr);
        }

    }

}
