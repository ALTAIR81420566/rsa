import java.math.BigInteger;
import java.security.SecureRandom;

/**
 * Created by altair on 13.01.17.
 */
public class RSA {

   private final static BigInteger one = new BigInteger("1");
   private final static SecureRandom random = new SecureRandom();

   private BigInteger privateKey;
   private BigInteger publicKey;
   private BigInteger modulus;

   public void init(int N) {
      BigInteger p = BigInteger.probablePrime(N, random);
      BigInteger q = BigInteger.probablePrime(N, random);
      BigInteger phi = (p.subtract(one)).multiply(q.subtract(one));

      modulus  = p.multiply(q);

      privateKey = publicKey.modInverse(phi);
   }

   public void setPrivateKey(BigInteger privateKey) {
      this.privateKey = privateKey;
   }

   public void setPublicKey(BigInteger publicKey) {
      this.publicKey = publicKey;
   }

   public void setModulus(BigInteger modulus) {
      this.modulus = modulus;
   }

   public BigInteger getPrivateKey() {
      return privateKey;
   }

   public BigInteger getPublicKey() {
      return publicKey;
   }

   public BigInteger getModulus() {
      return modulus;
   }

   RSA() {
      publicKey  = new BigInteger("65537");
   }

   BigInteger encrypt(BigInteger message) {
      return message.modPow(publicKey, modulus);
   }

   BigInteger decrypt(BigInteger encrypted) {
      return encrypted.modPow(privateKey, modulus);
   }

   public String toString() {
      String s = "";
      s += "public  = " + publicKey  + "\n";
      s += "private = " + privateKey + "\n";
      s += "modulus = " + modulus;

      return s;
   }

}